function isEmpty(str) {
    return str.trim() === '';
}

const TWO = 2;

let word = prompt('Enter your word: ');
let wordLength = word.length;
if (!isEmpty(word)) {
    if (wordLength % TWO === 0) {
        alert(word.substring(wordLength / TWO - 1, wordLength / TWO + 1));
    } else {
        alert(word.charAt(Math.floor(wordLength / TWO)));
    }
} else {
    alert('Empty field with word!');
}

