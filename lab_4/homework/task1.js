let num = prompt('Enter check number: ');
let checkNum = parseInt(num);
let percent = 0.01;
let percentLimit = 100;

if(checkNum && checkNum>0){
    const tip = prompt('Enter tip percentage: ');
    if (checkNum && tip > 0 && tip <=percentLimit){
        let tipAmount = checkNum * tip * percent;
        let totalSum = checkNum + tipAmount;
        alert('Check number:  ' + checkNum + '\n' +
        'Tip: ' + tip + '%' + '\n' +
        'Tip amount: ' + tipAmount + '\n' +
        'Total sum to pay: ' + totalSum);
    } else{
        alert('Wrong data for check percentage, you entered symbol or negative number or percentage >=100!!!');
    }
} else {
    alert('Wrong data for check number, you entered symbol or negative number!!!');
}